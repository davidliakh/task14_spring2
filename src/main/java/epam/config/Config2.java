package epam.config;

import epam.model.beans2.CatAnimal;
import epam.model.beans3.BeanE;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScans({
        @ComponentScan(basePackages = "epam.model.beans2",
                excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = CatAnimal.class)
        ),
        @ComponentScan(basePackages = "epam.model.beans3",
                excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = BeanE.class))
})
public class Config2 {
}

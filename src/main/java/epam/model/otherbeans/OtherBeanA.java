package epam.model.otherbeans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class OtherBeanA {
    private static final Logger logger = LogManager.getLogger();
    private String name;
    private int value;

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "OtherBeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}

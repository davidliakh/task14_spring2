package epam.model.beans1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanC {
    private static final Logger logger = LogManager.getLogger();
    private String name;
    private int value;

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}

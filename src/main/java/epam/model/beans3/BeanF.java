package epam.model.beans3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanF {
    private static final Logger logger = LogManager.getLogger();
    private String name;
    private int value;

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
